package main

import (
	"crypto/sha512"
	"database/sql"
	"encoding/hex"
	"fmt"
	"html/template"
	"net/http"

	"github.com/gorilla/sessions"

	_ "github.com/go-sql-driver/mysql"
)

var store = sessions.NewCookieStore([]byte("secret-key"))

type ViewData struct {
	Username string
}

func main() {

	http.HandleFunc("/static/", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "templates"+r.URL.Path)
	})

	db, err := sql.Open("mysql", "root@/users")

	if err != nil {
		fmt.Println(err)
	}
	defer db.Close()

	// Configure websocket route
	http.HandleFunc("/ws", HandleConnections)

	// Start listening for incoming chat messages
	go HandleMessages()

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		session, _ := store.Get(r, "session-name")
		check, err := session.Values["username"]

		if !err {
			fmt.Println("Ошибка при попытки аутентификации")
			http.Redirect(w, r, "/registration", http.StatusSeeOther)
			return
		} else {
			fmt.Println("Успешная аутентификация")

			if check != nil {

				var a string
				_ = db.QueryRow("SELECT username FROM users WHERE username = ?", check).Scan(&a)

				data := ViewData{
					Username: a,
				}

				tmpl, err := template.ParseFiles("templates/data.html")
				if err != nil {
					fmt.Print(err)
				}
				tmpl.Execute(w, data)
			}
		}
	})

	http.HandleFunc("/registration", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "templates/registration.html")
	})

	http.HandleFunc("/authorization", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "templates/authorization.html")
	})

	http.HandleFunc("/chat", func(w http.ResponseWriter, r *http.Request) {
		session, _ := store.Get(r, "session-name")
		check := session.Values["username"]

		if check == nil {
			fmt.Println("Ошибка авторизации при попытке открыть чат")
			http.Redirect(w, r, "/registration", http.StatusSeeOther)
			return
		} else {
			http.ServeFile(w, r, "templates/chat.html")
		}
	})

	http.HandleFunc("/log_out", func(w http.ResponseWriter, r *http.Request) {
		// Удаление сессии после выхода из аккаунта
		session, _ := store.Get(r, "session-name")
		session.Options.MaxAge = -1
		session.Save(r, w)

		http.Redirect(w, r, "/", http.StatusSeeOther)
	})

	http.HandleFunc("/postform", func(w http.ResponseWriter, r *http.Request) {

		username := r.FormValue("username")
		password := r.FormValue("password")

		var countReg int
		checkUnique := db.QueryRow("SELECT COUNT(*) FROM users WHERE username = ?", username).Scan(&countReg)
		if checkUnique != nil {
			fmt.Print("ERROR: ", checkUnique)
		}
		if countReg > 0 {
			http.Redirect(w, r, "/", http.StatusSeeOther)
		} else {

			hashSHA512 := sha512.Sum512([]byte(password))
			hash_pass := hex.EncodeToString(hashSHA512[:])

			_, err := db.Exec("INSERT INTO users (username, password) VALUES (?, ?)", username, hash_pass)
			if err != nil {
				fmt.Println(err)
				return
			}

			session, _ := store.Get(r, "session-name")
			session.Values["username"] = username
			session.Save(r, w)

			fmt.Println("Попытка создания аутентификации")
			http.Redirect(w, r, "/", http.StatusSeeOther)
		}
	})

	http.HandleFunc("/authorization_form", func(w http.ResponseWriter, r *http.Request) {
		log_username := r.FormValue("log_username")
		log_password := r.FormValue("log_password")

		hashSHA512 := sha512.Sum512([]byte(log_password))
		hash_log_pass := hex.EncodeToString(hashSHA512[:])

		var dbPassword string

		checkPass := db.QueryRow("SELECT password FROM users WHERE username = ?", log_username).Scan(&dbPassword)
		if checkPass != nil {
			fmt.Println(checkPass)
			http.Redirect(w, r, "/authorization", http.StatusSeeOther)
		}

		if hash_log_pass == dbPassword {
			fmt.Println("Пароли совпадают")

			session, _ := store.Get(r, "session-name")
			session.Values["username"] = log_username
			session.Save(r, w)

			fmt.Println("Попытка создания аутентификации после авторизации")

			http.Redirect(w, r, "/", http.StatusSeeOther)

		} else {
			fmt.Println("Пароли не совпадают")
			http.Redirect(w, r, "/authorization", http.StatusSeeOther)
		}
	})

	fmt.Println("Server is listening...")
	http.ListenAndServe("0.0.0.0:8000", nil)
}
