package main

import (
	"database/sql"
	"fmt"
	"net/http"

	"github.com/gorilla/websocket"

	_ "github.com/go-sql-driver/mysql"
)

var Clients = make(map[*websocket.Conn]bool) // connected clients
var Broadcast = make(chan Message)           // broadcast channel

type Message struct {
	Author string `json:"author"`
	Body   string `json:"body"`
}

var Upgrader = websocket.Upgrader{}

func HandleConnections(w http.ResponseWriter, r *http.Request) {
	// Upgrade initial GET request to a websocket
	ws, err := Upgrader.Upgrade(w, r, nil)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer ws.Close()

	// Register our new client
	Clients[ws] = true

	db, _ := sql.Open("mysql", "root@/users")
	rows, _ := db.Query("SELECT username, message FROM messages")
	for rows.Next() {
		var msg Message
		rows.Scan(&msg.Author, &msg.Body)
		ws.WriteJSON(msg)
	}

	session, _ := store.Get(r, "session-name")
	check := session.Values["username"]

	for {
		var msg Message = Message{Author: check.(string)}
		// Read in a new message as JSON and map it to a Message object
		err := ws.ReadJSON(&msg)
		if err != nil {
			delete(Clients, ws)
			break
		}
		// Send the newly received message to the broadcast channel
		Broadcast <- msg
	}
}

func HandleMessages() {
	db, _ := sql.Open("mysql", "root@/users")
	for {
		// Grab the next message from the broadcast channel
		msg := <-Broadcast
		// Send it out to every client that is currently connected
		for client := range Clients {
			err := client.WriteJSON(msg)
			if err != nil {
				client.Close()
				delete(Clients, client)
			}
		}
		fmt.Println(msg.Author, " ", msg.Body)
		_, err := db.Exec("INSERT INTO messages (username, message) VALUES (?, ?)", msg.Author, msg.Body)
		if err != nil {
			fmt.Println(err)
			return
		}
	}
}
